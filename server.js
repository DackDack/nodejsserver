'use strict';

const express = require('express');

// константы
const port = 8080;
const host = '0.0.0.0';

// приложение
const app = express();
app.get('/', (req, res) => {
  res.send('Hello World');
});

app.get('/timeout', (req, res) => {
  setTimeout(function(){
    res.send('Hello!!!');
  }, 3000);
});

app.listen(port, host);
console.log(`running on http://${host}:${port}`);
